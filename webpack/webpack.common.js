/* eslint-disable import/no-extraneous-dependencies */
const eslint = require('eslint');
const webpack = require('webpack');
const convert = require('koa-connect');
const history = require('connect-history-api-fallback');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const path = require('path');
const commonPaths = require('./paths');

module.exports = [
  {
    name: 'client',
    entry: commonPaths.entryPath,
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.(js|jsx)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            formatter: eslint.CLIEngine.getFormatter('stylish'),
            emitWarning: process.env.NODE_ENV !== 'production',
          },
        },
        {
          test: /\.(js|jsx)$/,
          loader: 'babel-loader',
          exclude: /(node_modules)/,
          options: {
            cacheDirectory: path.resolve(__dirname, '../', '.cache'),
          },
        },
        {
          test: /\.(js|jsx)$/,
          enforce: 'pre',
          use: ['source-map-loader'],
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: commonPaths.imagesFolder,
              },
            },
          ],
        },
        {
          test: /\.(woff2|ttf|woff|eot)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: commonPaths.fontsFolder,
              },
            },
          ],
        },
      ],
    },
    serve: {
      add: app => {
        app.use(convert(history()));
      },
      content: commonPaths.entryPath,
      dev: {
        publicPath: commonPaths.outputPath,
      },
      open: true,
    },
    resolve: {
      modules: ['src', 'node_modules'],
      extensions: ['*', '.js', '.jsx', '.css', '.scss'],
    },
    plugins: [
      new webpack.ProgressPlugin(),
      new HtmlWebpackPlugin({
        template: commonPaths.templatePath,
      }),
      new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: 'async',
      }),
    ],
  },
  {
    name: 'serviceWorker',
    entry: commonPaths.serviceWorker,
    output: {
      filename: `serviceWorker.js`,
      path: commonPaths.outputPath,
      chunkFilename: `serviceWorker.js`,
    },
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.(js|jsx)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            formatter: eslint.CLIEngine.getFormatter('stylish'),
            emitWarning: process.env.NODE_ENV !== 'production',
          },
        },
        {
          test: /\.(js|jsx)$/,
          loader: 'babel-loader',
          exclude: /(node_modules)/,
          options: {
            cacheDirectory: path.resolve(__dirname, '../', '.cache'),
          },
        },
      ],
    },
    resolve: {
      modules: ['src', 'node_modules'],
      extensions: ['*', '.js', '.jsx', '.css', '.scss'],
    },
    plugins: [new webpack.ProgressPlugin()],
  },
];
