const path = require('path');

const commonPaths = require('./paths');

module.exports = [
  {
    name: 'client',
    mode: 'development',
    output: {
      filename: '[name].js',
      path: commonPaths.outputPath,
      chunkFilename: '[name].js',
    },
    module: {
      rules: [
        {
          test: /\.(css|scss)$/,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                localsConvention: 'camelCase',
                modules: {
                  localIdentName: '[local]___[hash:base64:5]',
                },
              },
            },
            'sass-loader',
          ],
        },
      ],
    },
    devServer: {
      disableHostCheck: true,
      contentBase: [
        commonPaths.outputPath,
        path.resolve(__dirname, '../', 'static'),
      ],
      compress: true,
      host: 'localhost',
      port: 8080,
      // proxy: {
      //   '/': {
      //     changeOrigin: true,
      //     target: 'https://www.bestbuy.com',
      //     secure: true,
      //   },
      // },
    },
    plugins: [],
  },
  {
    name: 'serviceWorker',
    mode: 'development',
    devServer: {
      disableHostCheck: true,
      contentBase: commonPaths.outputPath,
      compress: true,
      host: 'localhost',
      port: 8080,
    },
  },
];
