import './set-public-path';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import { registerDewey, unregisterDewey } from './set-up-service-worker';
import { FILE_NAMES } from './constants';

const getRandomInt = max => Math.floor(Math.random() * Math.floor(max));

/**
 * Tests consecutive network requests.
 */
const consecutiveRequestsTest = async (num = 2000) => {
  for (let i = 0; i < num; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    const r = new Request(`/${FILE_NAMES[getRandomInt(FILE_NAMES.length)]}`);
    r.someField = 'value';
    await window.fetch(r, undefined, { cacheKey: 'hello' });
  }
};

// const original = Request.prototype.clone;
// Request.prototype.clone = function () {
//   const newRequest = original.call(this);
//   newRequest.customField = 'something';
//   return newRequest;
// };

const fetchFn = window.fetch;

window.fetch = (...args) => {
  const tags = args.length > 1 ? args[2] : {};
  return fetchFn(...args);
};

/**
 * Tests concurrent network requests.
 * WARNING: Setting this max to 500 caused my audio drivers to fail. Good luck.
 */
const concurrentRequestsTest = async (num = 300) => {
  const promises = [];
  for (let i = 0; i < num; i += 1) {
    promises.push(
      new Promise(resolve => {
        const r = new Request(
          `/${FILE_NAMES[getRandomInt(FILE_NAMES.length)]}`
        );
        resolve(window.fetch(r, undefined, { cacheKey: 'hello' }));
      })
    );
  }
  await Promise.all(promises);
};

class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      registered: null,
    };
  }

  async componentDidMount() {
    if (navigator.serviceWorker) {
      navigator.serviceWorker.getRegistrations().then(registrations =>
        this.setState({
          registered: registrations && !!registrations.length,
        })
      );
    }
  }

  registerServiceWorker() {
    registerDewey();
    this.setState({
      registered: true,
    });
  }

  unregisterServiceWorker() {
    unregisterDewey();
    this.setState({
      registered: false,
    });
  }

  async runConsecutiveRequestsTest() {
    const startTime = Date.now();

    await consecutiveRequestsTest();
    // await concurrentRequestsTest();

    const endTime = Date.now();
    const difference = endTime - startTime;
    this.setState({ startTime, endTime, difference });
  }

  async runConcurrentRequestsTest() {
    const startTime = Date.now();

    await concurrentRequestsTest();

    const endTime = Date.now();
    const difference = endTime - startTime;
    this.setState({ startTime, endTime, difference });
  }

  render() {
    return (
      <div>
        <h2>Service Worker Tests</h2>
        <div>{`${JSON.stringify(this.state)}`}</div>
        <button type="button" onClick={this.registerServiceWorker.bind(this)}>
          Register Service Worker
        </button>
        <button type="button" onClick={this.unregisterServiceWorker.bind(this)}>
          Unregister Service Worker
        </button>
        <button
          type="button"
          onClick={this.runConsecutiveRequestsTest.bind(this)}
        >
          Run Consecutive Requests Test
        </button>
        <button
          type="button"
          onClick={this.runConcurrentRequestsTest.bind(this)}
        >
          Run Concurrent Requests Test
        </button>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
